package com.hch;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CalculatorServiceTest {

  private CalculatorService underTest = new CalculatorService();

  @Test
  public void correctSum() {
    Integer result = underTest.sum(5, 4);
    assertThat(result).isEqualTo(9);
  }
}
