package com.hch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("calculator")
public class CalculatorController {

  @Autowired
  private CalculatorService calculatorService;

  @GetMapping
  public Integer sum(@RequestParam Integer a, @RequestParam Integer b) {
    return calculatorService.sum(a, b);
  }
}
