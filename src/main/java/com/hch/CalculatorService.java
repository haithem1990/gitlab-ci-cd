package com.hch;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

@Service
public class CalculatorService {

  public Integer sum(@RequestParam Integer a, @RequestParam Integer b) {
    return a + b;
  }

}
